Install

`` composer install arbitrary-torque/socialite-ape-farm ``

Add service provider to config/app.php $providers array
`` \ArbitraryTorque\Socialite\ServiceProvider::class ``

Add service config to config/services.php
```php
php artisan vendor:publish --provider='ArbitraryTorque\Socialite\ServiceProvider'
```

Add logic to your controllers

```php

public function redirectToProvider()
{
    return Socialize::with('ape-farm')->redirect();
}

public function handleProviderCallback()
{
    $user = Socialize::with('ape-farm')->user();

    // $user->token;
}

```