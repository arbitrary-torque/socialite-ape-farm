<?php

namespace ArbitraryTorque\Socialite;

use Laravel\Socialite\SocialiteManager;

/**
 * Class ApeFarmManager
 * @package ArbitraryTorque\Socialite
 */
class ApeFarmManager extends SocialiteManager
{
    /**
     * @return \Laravel\Socialite\Two\AbstractProvider
     */
    public function createApeFarmDriver()
    {
        $config = $this->app['config']['services.' . ApeFarmProvider::NAME];

        return $this->buildProvider(ApeFarmProvider::class, $config);
    }
}