<?php

namespace ArbitraryTorque\Socialite;

use Illuminate\Support\ServiceProvider as BaseProvider;

/**
 * Class ServiceProvider
 * @package ArbitaryTorque\Socialite
 */
class ApeFarmServiceProvider extends BaseProvider
{
    /**
     * @var bool
     */
    protected $defer = true;

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $socialite = $this->app->make('Laravel\Socialite\Contracts\Factory');

        $socialite->extend(

            ApeFarmProvider::NAME,

            function ($app) use ($socialite) {

                $config = $app['config']['services.' . ApeFarmProvider::NAME];

                return $socialite->buildProvider(ApeFarmProvider::class, $config);
            }

        );
    }

    public function boot()
    {
        $this->publishes([
            dirname(__FILE__) . '/config/services.php' => config_path('services.php'),
        ]);
    }

}