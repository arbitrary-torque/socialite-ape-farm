<?php

return [

    ArbitraryTorque\Socialite\ApeFarmProvider::NAME => [
        'client_id'     => env('OAUTH_APE_FARM_CLIENT_ID'),
        'client_secret' => env('OAUTH_APE_FARM_CLIENT_SECRET'),
        'redirect'      => 'https://my.app.farm/callback'
    ]

];
